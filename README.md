ElmahTray
=========


What is ElmahTray?
------------------

ElmahTray is a small Windows application written in C# that can help you observe an Elmah log. It shows the current log status in the system tray, and shows a notification when a new error appears in the Elmah log.

ElmahTray depends on ScriptCS, so you need to install it first: <http://scriptcs.net/>


How To
------

1. Install ScriptCS <http://scriptcs.net/>
2. Clone this repository
3. Open `start.csx` in a text editor
4. Set `elmahAxdPath` to point to an Elmah AXD endpoint (e.g. http://.../elmah.axd)
5. Open a command prompt and execute `scriptcs start.csx`
6. A tray icon should now appear in your system tray


About
-----
ElmahTray is developed by Regin Larsen, a .NET developer at [Eksponent](http://www.eksponent.com).


License
-------
ElmahTray is available under the MIT License.

Copyright (c) 2013 Regin Larsen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
