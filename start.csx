#r "System.Windows.Forms.dll"
#r "System.Drawing.dll"

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml.Linq;

public class ElmahTrayApp : Form
{	
	// Point to the Elmah endpoint
	private static string elmahAxdPath = "http://.../elmah.axd";
	
	// How often should we pool errors from Elmah?
	private static int updateIntervalSeconds = 60;
	
	private static NotifyIcon trayIcon;
	private ContextMenu trayMenu;
	private static int errorCount = 0;
	private static HashSet<string> knownErrors = new HashSet<string>();
	private static Timer timer = new Timer();

	[STAThread]
	public static void Main()
	{
		Application.Run(new ElmahTrayApp());
	}
	
	public ElmahTrayApp()
	{
		trayMenu = new ContextMenu();
		
		trayMenu.MenuItems.Add("Go to Elmah", OnGoToElmah);
		trayMenu.MenuItems.Add("Ignore Current Errors", OnResetErrors);
		trayMenu.MenuItems.Add("Exit", OnExit);

		trayIcon = new NotifyIcon();
		trayIcon.Text = "Elmah hasn't been checked yet";

		SetTrayIcon("grey.png", "Starting up ...");
		
		trayIcon.ContextMenu = trayMenu;
		trayIcon.Visible = true;
		
		timer.Tick += new EventHandler(CheckElmahLog);

		timer.Start();
	}

	protected override void OnLoad(EventArgs e)
	{
		Visible = false;
		ShowInTaskbar = false;

		base.OnLoad(e);
	}

	private void OnExit(object sender, EventArgs e)
	{
		timer.Stop();
		Application.Exit();
	}
	
	private void OnGoToElmah(object sender, EventArgs e)
	{
		Process.Start(elmahAxdPath);
	}
	
	private void OnResetErrors(object sender, EventArgs e)
	{
		errorCount = 0;
		SetTrayIcon("green.png", "No errors! #h5yr");
	}

	 private static void CheckElmahLog(Object obj, EventArgs args)
	 {
		timer.Stop();

		try
		{
			var document = XDocument.Load(elmahAxdPath + "/rss");

			var items = document.Root.Descendants("item").ToList();

			Console.WriteLine("Error count: " + items.Count);
			
			var newErrorsCount = 0;
			
			foreach(var item in items)
			{
				var link = item.Element("link").Value;
				if(!knownErrors.Contains(link))
				{
					knownErrors.Add(link);
					newErrorsCount++;
					errorCount++;
				}
			}
			
			if(errorCount > 4)
			{
				SetTrayIcon("red.png", string.Format("{0} errors. Sure everything is running smoothly?", errorCount));
			}
			else if(errorCount > 0)
			{
				SetTrayIcon("yellow.png", string.Format("Incoming ... {0} {1}", errorCount, errorCount == 1 ? "error" : "errors"));
			}
			else
			{
				SetTrayIcon("green.png", "No errors! #h5yr");
			}
			
			if(newErrorsCount > 0)
			{
				var title = string.Format("Oh bugger! {0} new {1} ...", newErrorsCount, newErrorsCount == 1 ? "error" : "errors");
				
				var newItems = items.Take(newErrorsCount).Select(i => "* " + Truncate(i.Element("description").Value, 80));
				var details = string.Join("\r\n", newItems);
				trayIcon.ShowBalloonTip(1000, title, details, ToolTipIcon.Error);
			}
			
		}
		catch(Exception e)
		{
			SetTrayIcon("grey.png", "Somethings up: " + e.Message);
			Console.WriteLine("Error: " + e.Message);
		}
		
		timer.Enabled = true;
		timer.Interval = updateIntervalSeconds * 1000;
	}
	
	private static void SetTrayIcon(string imagePath, string text)
	{
		var bitmap = new Bitmap(imagePath);
		var iconHandle = bitmap.GetHicon();
		var icon = System.Drawing.Icon.FromHandle(iconHandle);
		trayIcon.Icon = new Icon(icon, 40, 40);
		trayIcon.Text = Truncate(text, 63);
	}
	
	protected override void Dispose(bool isDisposing)
	{
		if (isDisposing)
		{
			trayIcon.Dispose();
		}

		base.Dispose(isDisposing);
	}
	
	private static string Truncate(string value, int maxLength)
	{
		return value.Length <= maxLength ? value : value.Substring(0, maxLength); 
	}
}

ElmahTrayApp.Main();